Gem::Specification.new do |s|
  s.name        = 'repatcher'
  s.version     = '1.2.1'
  s.date        = '2019-08-19'
  s.summary     = "patch maintenance tool"
  s.description = ""
  s.authors     = ["Iaroslav Gridin"]
  s.email       = 'voker57@gmail.com'
  s.executables = ["repatcher"]
  s.homepage    =
    'https://gitlab.com/nisec/repatcher'
  s.license       = 'MIT'
  s.add_runtime_dependency 'rugged', ['>= 0.24.0', "< 0.29.0"]
end
